This folder contains the R-markdown code and data files to perform the analysis of the paper entitled 
“Ocean warming threatens southern right whale population recovery”
Authors: Macarena Agrelo, Fábio G. Daura-Jorge, Victoria J. Rowntree, Mariano Sironi, Philip S. Hammond, Simon N. Ingram, Carina F. Marón, Florencia O. Vilches, Jon Seger, Roger Payne, Paulo C. Simões-Lopes

Questions can be addressed to maca.agrelo@gmail.com

Data S1. Encounter histories of +1-year-old southern right whale females from 1971 to 2017, Península Valdés, Argentina (femalesch.txt)

Data S2. Encounter histories of whales identified in their year of birth from 1971 to 2017, Península Valdés, Argentina (calves19712017ch.txt)

Data S3. Data used to build Figure 1C and 1D. Mean monthly SST and mean density (ind. m–2) of ﻿Antarctic krill (Euphausia superba) within the SW South Atlantic Ocean from 1970 to 2016 (Krill_and_SST_7030.txt). Data obtained from https://www.esrl.noaa.gov/psd/data/gridded/data.cobe.html and http://www.iced.ac.uk/science/krillbase.htm, respectively.

Data S4. R Markdown: code for modeling SRW survival and predictions of population trajectories (abh2823_R-markdown_code.html)